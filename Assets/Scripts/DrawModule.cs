﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class DrawModule {

	public Line DrawnLine{
		get{
			return _Line;
		}
	}
	
	public virtual Camera Camera{
		get{
			if(_MainCamera == null)
				_MainCamera = Camera.main;
			return _MainCamera;
		}
	}

	private Camera _MainCamera;
	protected Line _Line;

	public abstract Vector3 GetNextPoint();
	protected abstract void ValidateDrawing();
	

}
