﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FreeDrawModule : DrawModule {

	[Inject]
	private FreeDrawModule(){

	}  
	
	public override Vector3 GetNextPoint(){
		Vector3 position = Camera.ScreenToWorldPoint(Input.mousePosition);
		position = position + new Vector3(0,0,11);

		return position;
	}

	protected override void ValidateDrawing(){

	}

}
