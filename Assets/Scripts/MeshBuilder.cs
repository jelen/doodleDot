﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshBuilder{

	private static readonly string MATERIAL_PATH = "Shape_Material";

	private static MeshRenderer PrepareNewObject(out Mesh mesh){
		GameObject obj = new GameObject();
		MeshRenderer shapedObjectRenderer = obj.AddComponent<MeshRenderer>();
		MeshFilter shapedObjectFilter = shapedObjectRenderer.gameObject.AddComponent<MeshFilter>();

		mesh = new Mesh();
		mesh.name = "CustomShape";
		
		shapedObjectFilter.mesh = mesh;

		return shapedObjectRenderer;
	}

	
	private static List<int> CalculateTriangles(Vector3[] vertices){
		List<int> triangles = new List<int>();
		int rightIndex = vertices[0].x >= vertices[vertices.Length - 1].x ? vertices.Length - 1 : 0;
		int leftIndex = vertices[0].x >= vertices[vertices.Length - 1].x ? 0 :  vertices.Length - 1;
		int sign = vertices[0].x >= vertices[vertices.Length - 1].x ? -1 : 1;
		bool rightSide = vertices[0].x >= vertices[vertices.Length - 1].x ? false : true;

		while(rightIndex != leftIndex){
			if(rightSide){
				triangles.Add(rightIndex);
				triangles.Add(rightIndex + sign);
				triangles.Add(leftIndex);
				rightIndex += sign;
				rightSide = false;
			}else{
				triangles.Add(leftIndex);
				triangles.Add(rightIndex);
				triangles.Add(leftIndex - sign);
				leftIndex -= sign;
				rightSide = true;
			}
		}

		return triangles;
	}

	public static GameObject CreateShape(Vector3[] outline, Color[] vertexColors = null){
		List<Vector3> vertices = new List<Vector3>(outline);
		List<int> triangles;
		Mesh shape;
		MeshRenderer shapedObject;
		
		shapedObject = PrepareNewObject(out shape);
		triangles = CalculateTriangles(outline);
		
		shape.SetVertices(vertices);
		shape.triangles = triangles.ToArray();
		shape.RecalculateNormals();
		shape.colors = vertexColors;
		
		shapedObject.material = Resources.Load<Material>(MATERIAL_PATH);

		return shapedObject.gameObject;
	}

}
