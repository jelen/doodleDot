﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StraightLine : Line {

	private Vector3 _EndPoint;

	[Inject]
	private void Inject(){
		base.Init();
		Renderer.positionCount = 2;
	}
	
	public override void SetPoint(Vector3 point){
		_EndPoint = point;
		SetLine();
	}

	public override void SetStartingPoint(Vector3 point){
		Renderer.SetPosition(0, point);
	}

	public override void FinishLine(){

	}

	public override List<Color> CalculateColorDistribution(Color startColor, Color endColor){
		return new List<Color>(){startColor, endColor};
	}

	public override List<Vector3> GetPoints(){
		List<Vector3> points = new List<Vector3>();
		points.Add(Renderer.GetPosition(0));
		points.Add(_EndPoint);

		return points;
	}

	protected override void SetLine(){
		Renderer.SetPosition(1, _EndPoint);
	}

	public class Factory : Factory<StraightLine>{}
}
