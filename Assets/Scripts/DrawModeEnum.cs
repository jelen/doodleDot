﻿namespace DotDoodles.Core{
	public enum DrawMode{
		LINE,
		FREE_FLOW,
		LINE_DECORATOR,
		FREE_FLOW_DECORATOR
	}

}
