﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DotDoodles.Outline;

public class PictureController : MonoBehaviour {

	[Inject]
	private SimpleDot.Factory _DotFactory;

	[Inject]
	private PictureConfig _ConfigInstance;

	private List<SimpleDot> _PicturePoints;
	private List<Section> _Outline;
	private Color[] _Palette;
	private ScreenSegment[] _ScreenSegments;
	private Camera _Camera;
	private int _FinisheDots;


	[Inject]
	private void ControllerInitializer(){
		_PicturePoints = new List<SimpleDot>();
		_Outline = new List<Section>();
		_Palette = new Color[5]{Color.red, Color.green, Color.blue, Color.yellow, Color.magenta};
		_ScreenSegments = new ScreenSegment[_ConfigInstance.BigSegmentCount];
		_FinisheDots = 0;

		Msg.DotComplited += DotCompleted;
	}


	void Start(){
		_Camera = Camera.main;
	}

	public void LoadPicture(){
		//TODO::Finish 
		Debug.Log("Loading picture");
	}

	public void NewPicture(int dotCount){
		Color col;
		Vector3 pos;
		int connections;
		SimpleDot dot;

		for(int i = 0; i < dotCount; ++i){
			col = RollDotColor();
			pos = RollDotPosition();
			connections = Random.Range(2, 3);
			dot = _DotFactory.Create(connections, pos, col);
			//TODO::Check if dot's are proper distance from each other
			_PicturePoints.Add(dot);
		}

	}

	public void DeleteDots(){
		SimpleDot dot;

		for(int i = _PicturePoints.Count - 1; i >= 0; i--){
			dot = _PicturePoints[i];
			_PicturePoints.RemoveAt(i);
			Destroy(dot.gameObject);
		}
	}
	
	private Color RollDotColor(){
		int ptr = Random.Range(0, _Palette.Length);

		return _Palette[ptr];
	}

	private Vector3 RollDotPosition(){
		Vector3 result = new Vector3();
		Vector2 screenSize = _Camera.pixelRect.size;

		result.x = Random.Range(0, screenSize.x);
		result.y = Random.Range(0, screenSize.y);
		result.z = 11;

		return _Camera.ScreenToWorldPoint(result);
	}

	public void AddSection(List<Vector3> line, List<Color> colors){
		int fisrtIndexToDelete = -1;
		int secondIndexToDelete = -1;
		int index = 0;
		Section section = new Section();

		section.FirstEnd = line[0];
		section.SecondEnd = line[line.Count - 1];
		section.Line = line;
		section.VertexColors = colors;

		foreach(Section outlineElement in _Outline){
			if(Outline.CheckStartingPoints(outlineElement, section)){
				section = Outline.AddSection(outlineElement, section);
				
				if(fisrtIndexToDelete == -1)
					fisrtIndexToDelete = index;
				else{
					secondIndexToDelete = index;
					break;
				}
			}
			++index;
		}
		
		if(fisrtIndexToDelete >= 0){
			_Outline.RemoveAt(fisrtIndexToDelete);
			secondIndexToDelete -= 1;
		}
		if(secondIndexToDelete >= 0)
			_Outline.RemoveAt(secondIndexToDelete);
		
		_Outline.Add(section);
	}
	
	public void DrawOutline(){
		foreach(Section outline in _Outline)
			MeshBuilder.CreateShape(outline.Line.ToArray(), outline.VertexColors.ToArray());
		
		DeleteDots();
	}

/*
	public bool CheckForLineOverlap(Vector3 newPoint, Vector3 oldPoint){
		
	}
*/
	private void CheckPicture(){
		if(_FinisheDots == _PicturePoints.Count)
			DrawOutline();
	}

	private void DotCompleted(){
		_FinisheDots += 1;
		CheckPicture();
	}
}


//TODO::Move to seperate file
public class ScreenSegment{
	int Index; 
	int PixelWidth;
	int PixelHeight;
	Vector3 Center;	
	Vector2 PixelCenter;
	Vector2 LeftUpperCorner;
	Vector2 RightDownCorner;
	List<Vector3> LineSegemnt;

	public ScreenSegment(int segmentSize, int index){
		Camera currentCamera = Camera.main;
		int screenWidth = Screen.width;
		int screenHeight = Screen.height;
		int x = index % segmentSize;
		int y = index / segmentSize;
		LineSegemnt = new List<Vector3>();

		Index = index;
		PixelWidth = screenWidth/segmentSize;
		PixelHeight = screenHeight/segmentSize;
		PixelCenter = new Vector2(PixelWidth/2 * x, PixelHeight/2 * y);
		Center = currentCamera.ScreenToWorldPoint(PixelCenter);

	}
	
	public bool PointInBounds(Vector3 point){
		bool result = false;

		result = (point.x >= LeftUpperCorner.x) && (point.x < RightDownCorner.x);
		result = result && (point.y >= LeftUpperCorner.y) && (point.y < RightDownCorner.y);

		return result;
	}

}
