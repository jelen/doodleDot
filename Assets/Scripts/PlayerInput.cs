﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DotDoodles.Core;

public class PlayerInput : MonoBehaviour {

	public DrawMode Pen{
		get{
			return _Pen;
		}

		set{
			_Pen = value; 
		}
	}
	
	private Line _CurrentLine;
	private Camera _Camera; 
	private SimpleDot _StartingDot;
	private SimpleDot _EndingDot;
	private DrawMode _Pen;

	//TODO::Delete ;ater, for debug only
	private LineRenderer ShapeLine;

	#region Injected Fields
	[Inject]
	private FreeLine.Factory _FreeDraw;
	[Inject]
	private StraightLine.Factory _LineDraw;
	[Inject]
	private DrawModule _DrawHandler;
	[Inject]
	private PictureController _PictureController;
	#endregion



	// Use this for initialization
	void Start () {
		_Camera = Camera.main;

		Pen = DrawMode.LINE;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){

			if(! CheckForDot(out _StartingDot))
				return;

			if(_StartingDot.CanConnect == false)
				return;

			_CurrentLine = GetLine();
			_CurrentLine.SetStartingPoint(_StartingDot.transform.position);
			
		}

		if(Input.GetMouseButtonUp(0)){
			if(_CurrentLine == null)
				return;

			if(! CheckForDot(out _EndingDot)){
				Destroy(_CurrentLine.gameObject);
				return;
			}

			if(_EndingDot != null && _EndingDot.AttachLine() == false){
				Destroy(_CurrentLine.gameObject);
				return;
			}
			
			_StartingDot.AttachLine();
			_CurrentLine.SetPoint(_EndingDot.transform.position);
			_CurrentLine.FinishLine();

			_PictureController.AddSection(_CurrentLine.GetPoints(), _CurrentLine.CalculateColorDistribution(_StartingDot.Color, _EndingDot.Color));

			ShapeLine = _CurrentLine.Renderer;
			
			_CurrentLine = null;
		}

		//Drag handler
		if(Input.GetMouseButton(0)){
			if(_CurrentLine == null)
				return; 
			
			_CurrentLine.SetPoint(_DrawHandler.GetNextPoint());
		}

		if(Input.GetKeyDown(KeyCode.Alpha5)){
			_PictureController.DrawOutline();
		}

		if(Input.GetKeyDown(KeyCode.N)){
			_PictureController.NewPicture(5);
		}

		if(Input.GetKeyDown(KeyCode.D)){
			_PictureController.DeleteDots();
		}

		if(Input.GetKeyDown(KeyCode.F)){
			Pen = DrawMode.FREE_FLOW;
		}

		if(Input.GetKeyDown(KeyCode.L)){
			Pen = DrawMode.LINE;
		}
	}

	private bool CheckForDot(out SimpleDot dot){
		RaycastHit2D rayHit;
		rayHit = Physics2D.Raycast(_DrawHandler.GetNextPoint(), Vector2.down, 0.1f);
		
		if(rayHit.collider != null && rayHit.collider.gameObject.tag.Equals("Dot")){

			dot = rayHit.collider.GetComponent<SimpleDot>();
			dot.GetComponent<SpriteRenderer>().color = Color.black;
			return true;
		}
		else{
			dot = null;
			return false;	
		}
		
	}

	private Line GetLine(){
		switch(Pen){

			case DrawMode.LINE:
				return _LineDraw.Create();

			case DrawMode.FREE_FLOW:
				return _FreeDraw.Create();

			case DrawMode.FREE_FLOW_DECORATOR:
			case DrawMode.LINE_DECORATOR:
			default:
				return _FreeDraw.Create();
			
		}
	}

}
