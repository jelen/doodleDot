﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DotDoodles.Outline{
	public struct Section{
		public List<Vector3> Line;
		public List<Color> VertexColors;
		public Vector3 FirstEnd;
		public Vector3 SecondEnd;

	}

	/**
	 * Utillity class for operations on Sections
	 */
	public static class Outline{

		public static Section AddSection(Section firstSection, Section secondSection){
			Section merged = new Section();
			merged.Line = new List<Vector3>();

			if(firstSection.FirstEnd == secondSection.FirstEnd){
				merged.FirstEnd = firstSection.SecondEnd;
				merged.SecondEnd = secondSection.SecondEnd;
				firstSection.Line.Inverse();

			}else if(firstSection.SecondEnd == secondSection.SecondEnd){
				merged.FirstEnd = firstSection.FirstEnd;
				merged.SecondEnd = secondSection.FirstEnd;
				secondSection.Line.Inverse();

			}else if(firstSection.FirstEnd == secondSection.SecondEnd){
				merged.FirstEnd = firstSection.SecondEnd;
				merged.SecondEnd = secondSection.FirstEnd;
				firstSection.Line.Inverse();
				secondSection.Line.Inverse();

			}else{
				merged.FirstEnd = firstSection.FirstEnd;
				merged.SecondEnd = secondSection.SecondEnd;
			}

			merged.Line = firstSection.Line;
			merged.Line.AddRange(secondSection.Line);
			
			merged.VertexColors = firstSection.VertexColors;
			merged.VertexColors.AddRange(secondSection.VertexColors);

			return merged;
		}

		public static bool CheckStartingPoints(Section firstSection, Section secondSection){
			return  (firstSection.FirstEnd == secondSection.FirstEnd) ||
					(firstSection.FirstEnd == secondSection.SecondEnd) ||
					(firstSection.SecondEnd == secondSection.FirstEnd) ||
					(firstSection.SecondEnd == secondSection.SecondEnd);
		}

	}
	
}