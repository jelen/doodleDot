using DotDoodles.Core;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Extensions{

    public static void Inverse<T>(this List<T> list){
        T[] tmpArray = new T[list.Count];

        for(int i = 0; i < list.Count; i++){
            tmpArray[i] = list[i];
        }

        list.Clear();

        for(int i = tmpArray.Length - 1; i >= 0; i-- ){
            list.Add(tmpArray[i]);
        }
    }
}
