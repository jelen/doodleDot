﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class Line : MonoBehaviour{

	public LineRenderer Renderer{
		get{
			return _LineRenderer;
		}
	}

	private LineRenderer _LineRenderer;

	protected virtual void Init(){
		_LineRenderer = this.GetComponent<LineRenderer>();
	}

	protected abstract void SetLine();
	public abstract void SetPoint(Vector3 point);
	public abstract void SetStartingPoint(Vector3 point);
	public abstract void FinishLine();
	public abstract List<Vector3> GetPoints();
	public abstract List<Color> CalculateColorDistribution(Color startColor, Color endColor);

}
