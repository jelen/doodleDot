﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class InjectInstaller : MonoInstaller<InjectInstaller> {

	
	public override void InstallBindings(){
		BindControllers();
		BindSingletons();
		BindModules();
		BindViews();
		BindPrefabObjects();
		BindPictureConfigConstants();
	}

	private void BindControllers(){
		Container.Bind<PictureController>().FromNewComponentOnNewGameObject().WithGameObjectName("PictureController").UnderTransformGroup("Root").AsSingle().NonLazy();
	}

	private void BindModules(){
		Container.Bind<DrawModule>().To<FreeDrawModule>().AsSingle();
	}

	private void BindViews(){

	}

	private void BindPrefabObjects(){
		Container.BindFactory<FreeLine, FreeLine.Factory>().FromComponentInNewPrefabResource("Prefabs/Game/Line");
		Container.BindFactory<StraightLine, StraightLine.Factory>().FromComponentInNewPrefabResource("Prefabs/Game/Line");
		Container.BindFactory<int, Vector3, Color, SimpleDot, SimpleDot.Factory>().FromComponentInNewPrefabResource("Prefabs/Game/Dot");
	}

	private void BindSingletons(){
		Container.Bind<PictureConfig>().AsSingle();
	}

	private void BindPictureConfigConstants(){
		Container.BindInstance(4).WithId("HugeSegmentCount").WhenInjectedInto<PictureConfig>();
		Container.BindInstance(16).WithId("BigSegmentCount").WhenInjectedInto<PictureConfig>();
		Container.BindInstance(64).WithId("MediumSegmentCount").WhenInjectedInto<PictureConfig>();
		Container.BindInstance(256).WithId("SmallSegmentCount").WhenInjectedInto<PictureConfig>();
	}


}
