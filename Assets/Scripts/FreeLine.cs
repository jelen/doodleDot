﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FreeLine : Line {

	private List<Vector3> _LinePoints;

	private Vector3 _OldPoint;

	[Inject]
	private void Inject(){
		base.Init();
		_LinePoints = new List<Vector3>();
	}

	
	public override void SetPoint(Vector3 point){
		float delta = Vector3.Distance(point, _OldPoint);

		if(delta > 0.005){
			_LinePoints.Add(point);
			_OldPoint = point;
		}

		SetLine();
	}

	public override void SetStartingPoint(Vector3 point){
		_LinePoints.Add(point);
		_OldPoint = point;
		SetLine();
	}

	protected override void SetLine(){
		Renderer.positionCount = _LinePoints.Count;
		Renderer.SetPositions(_LinePoints.ToArray());

	}

	public override List<Vector3> GetPoints(){
		return _LinePoints;
	}

	public override void FinishLine(){
		Renderer.Simplify(0.02f);
		//this.gameObject.AddComponent<collider2d
		//TODO::Add catmull later and collider
	}
	
	public override List<Color> CalculateColorDistribution(Color startColor, Color endColor){
		float time = 0;
		float lineLength = Vector3.Distance(_LinePoints[0], _LinePoints[_LinePoints.Count - 1]);
		List<Color> colors = new List<Color>();
		for(int  i = 0; i < _LinePoints.Count; i++){
			float segmentLength = Vector3.Distance(_LinePoints[i], _LinePoints[_LinePoints.Count - 1]);
			time = 1 - (segmentLength/lineLength);
			colors.Add(Color.Lerp(startColor, endColor, time));
		}

		return colors;
	}

	public class Factory : Factory<FreeLine>{}
}
