﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SimpleDot : MonoBehaviour {

	private int _NumOfConnections;
	private int _ConnectionAllowance;
	private Color _DotColor;
	private List<Vector3> _LinePoints;
	private SpriteRenderer _Renderer;
	

	public Color Color{
		get{
			return _DotColor;
		}
	}

	public bool CanConnect{
		get{
			return (_NumOfConnections < _ConnectionAllowance);
		}
	}

	public bool AttachLine(){
		bool result;

		if(CanConnect){
			result = true;
			_NumOfConnections += 1;
			CheckCondition();
		}else{
			result = false;
		}

		return result;
	}

	[Inject]
	public void Init(int connections, Vector3 position, Color dotColor){
		_LinePoints = new List<Vector3>();
		_Renderer = this.GetComponent<SpriteRenderer>();
		_DotColor = dotColor;
		_ConnectionAllowance = connections;
		_NumOfConnections = 0;

		this.transform.position = position;
		_Renderer.color = _DotColor;
	}

	private void CheckCondition(){
		if(CanConnect == false)
			Msg.CallEvent(Event.DOT_COMPLETED);
		
		RefreshGraphics();
	}

	private void RefreshGraphics(){

	}


	public class Factory : Factory<int, Vector3, Color, SimpleDot>{

	}

}
